// https://medium.freecodecamp.org/build-your-first-vue-js-component-2dc204bca514

// PHP installatie
composer install
cp .env.example .env
php artisan key:generate

// .env aanpassen: DB_CONNECTION=sqlite en de rest van DB_*** verwijderen...
touch database/database.sqlite
php artisan migrate --seed

// Webpack watcher opstarten
npm install
npm run watch