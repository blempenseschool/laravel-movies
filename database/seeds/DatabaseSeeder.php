<?php

use App\Movie;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('movies')->truncate();

        $movies = [
            [
                'title' => 'Rings',
                'hasCounter' => true,
                'rating' => 3
            ],
            [
                'title' => 'Harry Potter and the Prisoner of Azkaban',
                'hasCounter' => true,
                'rating' => 4
            ],
            [
                'title' => 'Black Swan',
                'hasCounter' => false,
                'rating' => 2
            ],
            [
                'title' => 'Inferno',
                'hasCounter' => true,
                'rating' => 1
            ],
            [
                'title' => 'Inglourious Basterds',
                'hasCounter' => false,
                'rating' => 5
            ],
            [   
                'title' => 'Thor: The Dark World',
                'hasCounter' => true,
                'rating' => 4
            ],
            [
                'title' => 'Wind River',
                'hasCounter' => true,
                'rating' => 2
            ],
            [
                'title' => 'Fantastic Beasts and Where to Find Them',
                'hasCounter' => true,
                'rating' => 3
            ]            
        ];

        foreach($movies as $movie) {
            Movie::create($movie);
        }
    }
}
