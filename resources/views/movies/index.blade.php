<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" value="{{ csrf_token() }}">
  <title>Movies Rating</title>
  <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body >
  
  <div class="container mx-auto p-8" id="app">

  

  <h1 class="title">Movies</h1>

  @foreach($movies as $movie)
  <div class="border border-grey-light max-w-md p-4">
    <div class="mb-8">
      <div class="text-black font-bold text-xl mb-2">{{ $movie->title }}</div>
      <p class="text-grey-darker text-base">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.</p>
    </div>
    <div class="flex items-center">
      <rating></rating>
    </div>
  </div>
  @endforeach

  </div> 
  
  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>